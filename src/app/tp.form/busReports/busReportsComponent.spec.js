describe('Component: busReportsForm', function () {
    beforeEach(module('tp.form'));

    var element;
    var scope;
    beforeEach(inject(function($rootScope, $compile){
        scope = $rootScope.$new();
        element = angular.element('<bus-reports-form></bus-reports-form>');
        element = $compile(element)(scope);
        scope.$apply();
    }));

    it('should render the header', function() {
        var h3 = element.find('h3');
        expect(h3.text()).toBe('Trip Planner');
    });

    it('should render the accordion', function() {
        var h3 = element.find('.panel-default');
        expect(h3.length).toBe(2);
    });

    it('should render the accordion', function() {
        var h3 = element.find('.panel-default');
        expect(h3.length).toBe(2);
    });

});