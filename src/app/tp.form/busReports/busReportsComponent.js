(function () {
    'use strict';
    angular.module('tp.form').component('busReportsForm', {
        templateUrl: 'app/tp.form/busReports/busReportsComponent.html',
        controller: BusReportsComponent,
        controllerAs: 'busReportsComponentVm'
    }).filter('boldFilter', function () {
        return function (input, no) {
            return input.replace(RegExp('(' + input.substring(0, no) + ')', 'g'), '<b>$1</b>');
        }
    }).filter('statusFilter', function () {
        return function (input) {
            if (input >= 300) {
                return  '<span class="error">Late</span>';
            } else if (input < 0) {
                return '<span class="warning">Early</span>';
            } else {
                return '<span class="success">On Time</span>';
            }
        }
    }).filter('searchBoldfilter', function() {
        return function (input) {
            return  '<span class="alert alert-danger">'+input+'</span>';
        }
    });;
    BusReportsComponent.$inject = ['busDataService'];
    function BusReportsComponent(busDataService) {
        var busReportsComponentVm = this;
        busReportsComponentVm.results = [];
        busReportsComponentVm.$onInit = function () {
            busDataService.getBusData().then(
                function (response) {
                    console.log(response.data)
                    busReportsComponentVm.results = response.data;
                }).catch(function (error) {
                console.log('error occurred')
            })
        }
    }
})();
